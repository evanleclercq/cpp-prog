#include <iostream>
#include <string>

using namespace std;

void exercise1 ();
void exercise2 ();

void variableType (string);
void variableType (int);
void variableType (double);

void addition (double, double);
void addition (double, double, double);
void addition (double, double, double, double);
void addition (double, double, double, double, double);

int main ()
{
    
    exercise1();
    exercise2();
    
    return 0;
    
}

void exercise1 ()
{
    
    int i = 56;
    double d = 5.236;
    string s = "Hello World";
    
    variableType(s);
    variableType(i);
    variableType(d);
    
}

void exercise2 ()
{
    
    int n;
    
    cout << "How many values should get added together (2 to 5)? ";
    cin >> n;
    
    double vals[n];
    cout << "Please enter the values: ";
    
    for (int i = 0; i < n; i++) {
        cin >> vals[i];
    }
    
    switch (n) {
        case 2:
            addition(vals[0], vals[1]);
            break;
        case 3:
            addition(vals[0], vals[1], vals[2]);
            break;
        case 4:
            addition(vals[0], vals[1], vals[2], vals[3]);
            break;
        case 5:
            addition(vals[0], vals[1], vals[2], vals[3], vals[4]);
            break;
    }
    
}

void variableType (string s)
{
    cout << s << " - The variable was a String" << endl;
}

void variableType (int i)
{
    cout << i << " - The variable was an Integer" << endl;
}

void variableType (double d)
{
    cout << d <<  " - The variable was a Double" << endl;
}

void addition (double n, double m)
{
    cout << n << " + " << m << " = " << (n + m) << endl;
}

void addition (double n, double m, double o)
{
    cout << n << " + " << m << " + " << o << " = " << (n + m + o) << endl;
}

void addition (double n, double m, double o, double p)
{
    cout << n << " + " << m << " + " << o << " + " << p << " = " << (n + m + o + p) << endl;
}

void addition (double n, double m, double o, double p, double q)
{
    cout << n << " + " << m << " + " << o << " + " << p << " + " << q << " = " << (n + m + o + p + q) << endl;
}