#include <iostream>
using namespace std;

void addition ();
void subtraction ();
void multiplication ();
void division ();
void exponentiation ();
int chooseOperator ();
void floatingPoints ();
void minValue(double[5]);
void maxValue(double[5]);

int main ()
{
    
    int choice = chooseOperator();
    
    switch (choice)
    {
        
        case 1:
            addition();
            break;
        case 2:
            subtraction();
            break;
        case 3:
            division();
            break;
        case 4:
            multiplication();
            break;
        case 5:
            exponentiation();
            break;
        case 6:
            floatingPoints();
            break;
        default:
            cout << "ERROR! Invalid Selection Made. Program Ending!" << endl;
        
    }
    
    return 0;
    
}

void addition ()
{
    
    double a, b;
    
    cout << "First Number: ";
    cin >> a;
    cout << "Second Number: ";
    cin >> b;
    
    cout << a << " + " << b << " = " << (a + b) << endl;
    
}

void subtraction ()
{
    
    double a, b;
    
    cout << "First Number: ";
    cin >> a;
    cout << "Second Number: ";
    cin >> b;
    
    cout << a << " - " << b << " = " << (a - b) << endl;
    
}

void multiplication () 
{
    
    double a, b;
    
    cout << "First Number: ";
    cin >> a;
    cout << "Second Number: ";
    cin >> b;
    
    cout << a << " * " << b << " = " << (a * b) << endl;
    
}

void division ()
{
    
    double a, b;
    
    cout << "First Number: ";
    cin >> a;
    cout << "Second Number: ";
    cin >> b;
    
    cout << a << " / " << b << " = " << (a / b) << endl;
    
}

void exponentiation ()
{
    
    double a, b;
    
    cout << "Base Number: ";
    cin >> a;
    cout << "Exponent Number: ";
    cin >> b;
    
    int ans = a;
    
    for (int i = 1; i < b; i++){
        
        ans = ans * a;
        
    }
    
    cout << a << " to the power of " << b << " = " << ans << endl;
    
}

int chooseOperator ()
{
    
    int choice;
    
    cout << "Please choose your desired operator:" << endl;
    cout << "1 - Addition" << endl;
    cout << "2 - Subtraction" << endl;
    cout << "3 - Division" << endl;
    cout << "4 - Multiplication" << endl;
    cout << "5 - Exponential" << endl;
    cout << "6 - Floating Point Numbers"<< endl;
    cout << "Selection: ";
    cin >> choice;
    
    return choice;
    
}

void floatingPoints ()
{
    
    double arr[5];
    int choice;
    
    cout << "Please Enter your five foating point numbers: " << endl;
    cin >> arr[0] >> arr[1] >> arr[2] >> arr[3] >> arr[4];
    
    cout << "Please choose operation: " << endl;
    cout << "1 - Max Value" << endl;
    cout << "2 - Min Value" << endl;
    cin >> choice;
    cout << endl;
    
    switch (choice)
    {
        
        case 1:
            maxValue (arr);
            break;
        case 2:
            minValue (arr);
            break;
        default:
            cout << "ERROR! Invalid Selection. Program Ending!" << endl;
            break;
        
    }
    
}

void minValue (double arr[5])
{
    
    double minVal = 1000;
    
    for (int i = 0; i < 5; i++)
    {
        if (arr[i] < minVal)
        {
            minVal = arr[i];
        }
    }
    
    cout << "The minimum value was: " << minVal << endl;
    
}

void maxValue (double arr[5])
{
    
    double maxVal = -1000;
    
    for (int i = 0; i < 5; i++)
    {
        if (arr[i] > maxVal) {
            maxVal = arr[i];
        }
    }
    
    cout << "The maximum value was: " << maxVal << endl;
    
}