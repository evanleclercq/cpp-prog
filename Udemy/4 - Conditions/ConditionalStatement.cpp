#include <iostream>
using namespace std;

void exercise1 ();
void exercise3 ();

int main ()
{

    exercise1 ();
    cout << endl;
    exercise3 ();

    return 0;

}

void exercise1 ()
{

    int hi, low, check;

    cout << "User Inputted:" << endl;
    cout << "Minimum Value: ";
    cin >> low;
    cout << "Maximum Value: ";
    cin >> hi;
    cout << "Value to Check: ";
    cin >> check;

    if (hi < low)
    {
        cout << "ERROR! Max value is less the Min Value!" << endl;
    }
    else if (low > hi)
    {
        cout << "ERROR! Min value is greater than Max value!" << endl;
    }
    else
    {
        if (check > low && check < hi)
        {
            cout << "The number " << check << " belongs to the interval from " << low << " to " << hi << endl;
        }
        else
        {
            cout << "The number " << check << " does not belong to the interval from " << low << " to " << hi << endl;
        }
    }

}

void exercise3 ()
{

    int choice;
    string item;

    cout << "Please make your selection:" << endl;
    cout << "1. Cola" << endl;
    cout << "2. Chocolate Bar" << endl;
    cout << "3. Potato Chips" << endl;
    cout << "4. Orange Juice" << endl;
    cout << "5. Bottled Water" << endl;
    cout << "6. Slice" << endl;
    cout << "Selection: ";
    cin >> choice;

    if (choice == 1)
    {
        item = "Cola";
    }
    else if (choice == 2)
    {
        item = "Chocolate Bar";
    }
    else if (choice == 3)
    {
        item = "Potato Chips";
    }
    else if (choice == 4)
    {
        item = "Orange Juice";
    }
    else if (choice == 5)
    {
        item = " Bottled Water";
    }
    else if (choice == 6)
    {
        item = "Slice";
    }
    else
    {
        item = "Invalid Selection";
    }

    cout << "You have chosen: " << item << endl;

}
