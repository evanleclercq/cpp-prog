#include <iostream>
using namespace std;

void exercise1 ();
void exercise2 ();

int main ()
{

    exercise1 ();
    cout << endl;
    exercise2 ();

    return 0;

}

void exercise1 ()
{

    int age;
    string msg;

    cout << "Pleas Enter Your Age: ";
    cin >> age;

    msg = (age >= 18) ? "You are an adult!" : "You are not an adult!";

    cout << msg << endl;

}

void exercise2 ()
{

    int mark;
    string msg;

    cout << "Please Enter Your Mark: ";
    cin >> mark;

    if ( mark < 100)
    {
        msg = (mark > 50) ? "You passed the exam!" : "You didn't pass the exam!";
    }
    else
    {
        msg = "ERROR: The number of points is invalid!";
    }

    cout << msg << endl;

}
