#include <iostream>
using namespace std;

void exercise1 ();
void exercise2 ();

int main ()
{

    exercise1 ();
    cout << endl;
    exercise2 ();

    return 0;

}

void exercise1 ()
{

    int choice;
    string item;

    cout << "Please make your selection:" << endl;
    cout << "1. Cola" << endl;
    cout << "2. Chocolate Bar" << endl;
    cout << "3. Potato Chips" << endl;
    cout << "4. Orange Juice" << endl;
    cout << "5. Bottled Water" << endl;
    cout << "6. Slice" << endl;
    cout << "Selection: ";
    cin >> choice;

    switch (choice)
    {
        case 1:
            cout << "You have chosen a Chocolate Bar." << endl;
            break;
        case 2:
            cout << "You have chosen a Cola." << endl;
            break;
        case 3:
            cout << "You have chosen a Potato Chips." << endl;
            break;
        case 4:
            cout << "You have chosen a Orange Juice." << endl;
            break;
        case 5:
            cout << "You have chosen a Bottled Water." << endl;
            break;
        case 6:
            cout << "You have chosen a Slice." << endl;
            break;
        default:
            cout << "Your selection was invalid." << endl;
            break;
    }

}

void exercise2 ()
{

    int month;

    cout << "Please Enter a Month: ";
    cin >> month;

    switch (month) {
        case 1:
            cout << "The month on Janurary has 31 days." << endl;
            break;
        case 2:
            int year;

            cout << "In which year? ";
            cin >> year;

            if (year % 4 == 0)
            {
                cout << "The month February has 29 days in the year " << year << endl;
            }
            else
            {
                cout << "The month February has 28 days in the year " << year << endl;
            }
            break;
        case 3:
            cout << "The month on March has 31 days." << endl;
            break;
        case 4:
            cout << "The month on April has 30 days." << endl;
            break;
        case 5:
            cout << "The month on May has 31 days." << endl;
            break;
        case 6:
            cout << "The month on June has 30 days." << endl;
            break;
        case 7:
            cout << "The month on July has 31 days." << endl;
            break;
        case 8:
            cout << "The month on August has 31 days." << endl;
            break;
        case 9:
            cout << "The month on September has 30 days." << endl;
            break;
        case 10:
            cout << "The month on October has 31 days." << endl;
            break;
        case 11:
            cout << "The month on November has 30 days." << endl;
            break;
        case 12:
            cout << "The month on December has 31 days." << endl;
            break;
    }

}
