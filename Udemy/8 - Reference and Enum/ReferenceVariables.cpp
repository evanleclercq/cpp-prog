#include <iostream>
using namespace std;

void exercise1();

int main () {
    
    exercise1();
    
    return 0;
    
}

void exercise1() {
    
    string name = "Archibald";
    string & greg = name;
    string & lucy = name;
    string & dog = name;
    
    cout << "var name = " << name << " " << &name << endl;
    cout << "var greg = " << greg << " " << &greg << endl;
    cout << "var lucy = " << lucy << " " << &lucy << endl;
    cout << "var dog = " << dog << " " << &dog << endl;
    
}