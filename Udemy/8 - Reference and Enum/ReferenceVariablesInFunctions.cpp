#include <iostream>
using namespace std;

void exercise1();
void exercise2();

void setValue (int&);
void checkIfTheSame(int&, int&);

int main () {
    
    //exercise1();
    exercise2();
    
    return 0;
    
}

void exercise1() {
    
    int i;
    
    cout << "EXERCISE 1" << endl;
    setValue(i);
    
    cout << i << endl;
    
}

void exercise2() {
    
    int a = 10;
    int b = 15;
    int c = 10;
    int & d = a;
    
    checkIfTheSame(a, b);
    cout << endl;
    checkIfTheSame(a, c);
    cout << endl;
    checkIfTheSame(a, d);
    
}

void setValue (int & input) {
    
    cout << "Please enter a value: ";
    cin >> input;
    
}

void checkIfTheSame (int & var1, int & var2) {
    
    if (var1 == var2) {
        cout << "The two variables contain the same value." << endl;
    }
    
    if (var1 == var2 && &var1 == &var2) {
        cout << "The two variables have the same value and address." << endl;
    }
    
    if (var1 != var2 && &var1 != &var2) {
        cout << "The two variables are completely different." << endl;
    }
    
    if (var1 == var2) {
        if (&var1 != &var2) {
            cout << "The two variables have the same value but not the same address" << endl;
        }
    }
    
}