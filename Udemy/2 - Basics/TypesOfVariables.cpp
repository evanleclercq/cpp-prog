#include <iostream>
using namespace std;

void exercise1 ();
void exercise2 ();

int main ()
{

    exercise1();
    exercise2();

    return 0;

}

void exercise1 ()
{

    string a, b, c;

    cout << "Enter First String: ";
    cin >> a;
    cout << "Enter Second String: ";
    cin >> b;
    cout << "Ente Third String: ";
    cin >> c;

    cout << "Your three strings were: " << a << ", " << b << " and " << c << endl;

}

void exercise2 ()
{

    int a, b, temp;

    cout << "Enter First Number: ";
    cin >> a;
    cout << "Enter Second Number: ";
    cin >> b;

    cout << "The two numbers entered were a = " << a << " and b = " << b << endl;

    temp = a;
    a = b;
    b = temp;

    cout << "After swapping a = " << a << " and b = " << b << endl;

}
