#include <iostream>
using namespace std;

void exercise1 ();
void exercise2 ();
void exercise3 ();

int main ()
{

    exercise1 ();
    cout << endl;
    exercise2 ();
    cout << endl;
    exercise3 ();

    return 0;

}

void exercise1 ()
{

    cout << "There is no output from this exercise!" << endl;

}

void exercise2 ()
{

    int a = 9;
    double d = 3.54;
    float f = 4.321;
    bool isTrue = true;
    char c = 'f';
    string s = "This is a string";

    cout << "Variable a value = " << a << ", address = " << &a << endl;
    cout << "Variable d value = " << d << ", address = " << &d << endl;
    cout << "Variable f value = " << f << ", address = " << &f << endl;
    cout << "Variable isTrue value = " << isTrue << ", address = " << &isTrue << endl;
    cout << "Variable c value = " << c << ", address = " << &c << endl;
    cout << "Variable s value = " << s << ", address = " << &s << endl;

}

void exercise3 ()
{

    int roomNo = 102, floor = 1;
    string fname = "Evan", lname = "LeClercq";
    bool mealsInc = true;

    cout << "Room Number: " << roomNo << endl;
    cout << "Floor Number: " << floor << endl;
    cout << "Customer Name: " << fname << " " << lname << endl;
    cout << "Meals Included: " << mealsInc << endl;

}
