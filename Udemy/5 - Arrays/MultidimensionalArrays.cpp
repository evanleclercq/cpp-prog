#include <iostream>
using namespace std;

int main ()
{

    int arr[3][3];
    int x, y;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            arr[i][j] = (i+j) * 14;
        }
    }

    cout << "Please enter x co-ordinate (1-3): ";
    cin >> x;
    cout << "Please enter y co-ordinate (1-3): ";
    cin >> y;

    cout << "Number at given co-ordinates = " << arr[x-1][y-1] << endl;

    return 0;

}
