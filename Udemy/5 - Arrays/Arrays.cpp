#include <iostream>
using namespace std;

int main ()
{

    double arr[4];

    cout << "User Inputted:" << endl;
    cout << "Val 1: ";
    cin >> arr[0];
    cout << "Val 2: ";
    cin >> arr[1];
    cout << "Val 3: ";
    cin >> arr[2];
    cout << "Val 4: ";
    cin >> arr[3];

    int ans = arr[0] + arr[1] + arr[2] + arr[3];

    cout << endl << "Result: " << ans << endl;
    cout << endl << "Address of first element = " << &arr[0] << endl;
    cout << "Address of last element = " << &arr[3] << endl;

    return 0;

}
