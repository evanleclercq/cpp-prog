#include <iostream>
using namespace std;

void exercise1 ();
void exercise2 ();
void exercise3 ();
void exercise4 ();
void exercise5 ();
void exercise6 ();
void exercise7 ();

int main ()
{

    exercise1();
    cout << endl;
    exercise2();
    cout << endl;
    exercise3();
    cout << endl;
    exercise4();
    cout << endl;
    exercise5();
    cout << endl;
    exercise6();
    cout << endl;
    exercise7();
    cout << endl;

    return 0;

}


void exercise1 ()
{

    int inches, cm;

    cout << "EXERCISE 1" << endl << "------------------------------" << endl;
    cout << "Convert Inches to Centimetres:" << endl;
    cout << "Enter the number of inches to convert: ";
    cin >> inches;

    cm = inches * 2.54;

    cout << inches << " in centimetres = " << cm << endl;

}

void exercise2 ()
{

    int celsius, fahrenheit;

    cout << "EXERCISE 2" << endl << "------------------------------" << endl;
    cout << "Convert Celsius to Fahrenheit:" << endl;
    cout << "Enter the number of degrees celsius to convert: ";
    cin >> celsius;

    fahrenheit = (celsius * 1.8) + 32;

    cout << celsius << "degrees celsius equals " << fahrenheit << " degrees fahrenhiet" << endl;

}

void exercise3 ()
{

    int a, b;

    cout << "EXERCISE 3" << endl << "------------------------------" << endl;
    cout << "Enter Number 1: ";
    cin >> a;
    cout << "Enter Number 2: ";
    cin >> b;
    cout << a << " + " << b << " = " << (a + b) << endl;

}

void exercise4 ()
{
    int a, b;

    cout << "EXERCISE 4" << endl << "------------------------------" << endl;
    cout << "Enter Number 1: ";
    cin >> a;
    cout << "Enter Number 2: ";
    cin >> b;
    cout << a << " - " << b << " = " << (a - b) << endl;

}

void exercise5 ()
{
    int a, b;

    cout << "EXERCISE 5" << endl << "------------------------------" << endl;
    cout << "Enter Number 1: ";
    cin >> a;
    cout << "Enter Number 2: ";
    cin >> b;
    cout << a << " * " << b << " = " << (a * b) << endl;

}

void exercise6 ()
{
    int a, b;

    cout << "EXERCISE 6" << endl << "------------------------------" << endl;
    cout << "Enter Number 1: ";
    cin >> a;
    cout << "Enter Number 2: ";
    cin >> b;
    cout << a << " / " << b << " = " << (a / b) << endl;

}

void exercise7 ()
{
    int a, b;

    cout << "EXERCISE 7" << endl << "------------------------------" << endl;
    cout << "Enter Number 1: ";
    cin >> a;
    cout << "Enter Number 2: ";
    cin >> b;
    cout << a << " % " << b << " = " << (a % b) << endl;

}
