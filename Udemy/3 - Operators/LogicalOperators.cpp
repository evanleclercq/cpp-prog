#include <iostream>
using namespace std;

int main ()
{

    int hi, low, check;

    cout << "User Inputted:" << endl;
    cout << "Minimum Value: ";
    cin >> low;
    cout << "Maximum Value: ";
    cin >> hi;
    cout << "Value to Check: ";
    cin >> check;

    bool isValid = (check > low && check < hi) ? true : false;

    cout << "Is the number " << check << " contained in the interval from " << low << " to " << hi << "? " << isValid << endl;

    return 0;

}
