#include <iostream>
using namespace std;

int main ()
{

    int hi, low, check;

    cout << "User Inputted:" << endl;
    cout << "Minimum Value: ";
    cin >> low;
    cout << "Maximum Value: ";
    cin >> hi;
    cout << "Value to Check: ";
    cin >> check;

    bool isGreater = check >= low ? true : false;
    bool isLess = check <= hi ? true : false;

    cout << "Is the value " << check << " greater or equal to min value? " << isGreater << endl;
    cout << "Is the value " << check << " lower or equal to max value? " << isLess << endl;

    return 0;

}
