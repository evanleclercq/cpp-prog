#include "Book.h"

int Book::numberOfBooks = 0;

std::string Book::getTitle() const {
    return this->title;
}

std::string Book::getAuthor () const {
    return this->author;
}

int Book::getYear () const {
    return this->publicationYear;
}

int Book::getBookID () const {
    return this->bookID;
}

Book::Book (const Book& bookToCopy) {
    this->title = bookToCopy.getTitle();
    this->author = bookToCopy.getAuthor();
    this->publicationYear = bookToCopy.getYear();
    this->bookID = Book::numberOfBooks++;
}

Book::Book (std::string author, std::string title, int publicationYear) {
    this->author = author;
    this->title = title;
    this->publicationYear = publicationYear;
    this->bookID = Book::numberOfBooks++;
}

Book::Book (){
    
}

Book::~Book() {
    
}