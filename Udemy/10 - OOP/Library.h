#ifndef LIBRARY_H_INCLUDED
#define LIBRARY_H_INCLUDED

#include "Book.h"

class Library {
    
    private:
        int maxAmountOfBooks;
        int currentAmountOfBooks;
        Book * books;
        
    public:
        Library (int);
        ~Library ();
        
        friend void Librarian:lendBook (Library &, Borrower &, int);
        
        void addBook (Book);
        Book getBook (int);
        void showBooks();

};

#endif // LIBRARY_H_INCLUDED