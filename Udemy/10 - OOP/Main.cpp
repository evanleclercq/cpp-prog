#include <iostream>
#include <conio.h>
#include <cstdlib>

#include "Library.h"
#include "Book.h"
#include "User.h"

using namespace std;

int main () {
    
    const Book book1 ("Arkadiusz Wlodarczyk", "C++ course", 2009);
    const Book book2 ("J.R.R Tolkien", "The Hobbit", 1937);
    const Book book3 ("Andrzej Sapkowski", "Blood of Elves", 1994);
    const Book book4 (book3);
    const Book book5 (book3);
    
    Library library (5);
    Librarian librarian ("Foo", "Bar", 5);
    Borrower borrower ("John", "Doe", 5);
    
    library.addBook (book1);
    library.addBook (book2);
    library.addBook (book3);
    library.addBook (book4);
    library.addBook (book5);
    
    int position;
    char choice;
    book cont = true;
    
    while (cont) {
        
        library.showBooks();
        borrower.showBooks();
        
        cout << "Would you like to borrow - B/b or Return - R/r: ";
        cin >> choice;
        
        switch (choice) {
            
            case 'b':
            case 'B':
                cout << "Which book would you like to rent, POSITION IN LIBRARY: ";
                cin >> position;
                librarian.lendBook (library, borrower, position);
                break;
                
            case 'r':
            case 'R':
                cout << "Which book would you like to return, POSITION IN LIBRARY: ";
                cin >> position;
                borrower.returnBook (library, position);
                break;
            case 'q':
            case 'Q':
                cont = false;
                break;
            default:
                break;
        }
        
        system ("cls");
        
    }
    
    return 0;
    
}