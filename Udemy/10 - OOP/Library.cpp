#include "Library.h"

void Library::addBook (Book book) {
    
    if (this->currentAmountOfBooks < maxAmountOfBooks) {
        this->books[currentAmountOfBooks] = book;
        this->currentAmountOfBooks++;
    }
    
}

Book Library::getBook (int index) {
    
    if (index < this->maxAmountOfBooks) {
        return books[index];
    } else {
        return books[0];
    }
    
}

void Library::showBooks () {
    
    for (int i = 0; i < this->currentAmountOfBooks; i++) {
        
        std::cout << "Book in position " << i << std::endl;
        std::cout << "Title: " << getBook(i).getTitle() << std::endl;
        std::cout << "Author: " << getBook(i).getAuthor() << std::endl;
        std::cout << "Publication Year: " << getBook(i).getYear() << std::endl;
        std::cout << "Book ID: " << getBook(i).getBookID() << std::endl;
        std::cout << std::endl;
    }
    
}

Library::Library (int maxAmountOfBooks) {
    
    this->maxAmountOfBooks = maxAmountOfBooks;
    this->currentAmountOfBooks = 0;
    this->books = new Book[maxAmountOfBooks];
    
}

Library::~Library () {
    
    delete[] books;
    
}