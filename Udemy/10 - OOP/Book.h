#ifndef BOOK_H_INCLUDED
#define BOOK_H_INCLUDED

#include <iostream>

class Book {
    
    private:
        std::string title;
        std::string author;
        int publicationYear;
        int bookID;
        
    public:
        
        static int numberOfBooks;

        //Getters
        std::string getTitle () const;
        std::string getAuthor () const;
        int getYear () const;
        int getBookID () const;
    
        Book (const Book&);
        Book (std::string, std::string, int);
        Book();
        ~Book();
    
};

#endif //BOOK_H_INCLUDED