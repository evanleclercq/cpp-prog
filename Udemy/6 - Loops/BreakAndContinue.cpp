#include <iostream>
using namespace std;

void exercise1();
void exercise2();

int main ()
{
    
    exercise1();
    exercise2();
    
    return 0;
    
}

void exercise1() {
    
    int n;
    int total = 0;
    
    cout << "Please enter a number: ";
    cin >> n;
    
    for (int i = 1; i <= n; i++) {
        
        if (i % 2 != 0) {
            continue;
        } else {
            cout << total << " + " << i << " = " << total+i << endl;
            total += i;
        }
        
    }
    
}

void exercise2() {
    
    int sum = 0;
    int increment = 1;
    char c;
    
    do {
        
        cout << "The current sum is: " << sum << endl;
        cout << "Do you want to add " << increment << endl << endl;
        cout << "User Input: ";
        cin >> c;
        
        if (c == 'y' || c == 'Y') {
            
            sum += increment;
            increment = 1;
            continue;
            
        } else if (c == 'n' || c == 'N') {
            
            increment++;
            continue;
            
        } else {
            
            break;
            
        }
        
    } while (true);
    
}