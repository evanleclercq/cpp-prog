#include <iostream>
using namespace std;

void exercise1 ();
void exercise2 ();
void exercise3 ();
void exercise4 ();
void exercise5 ();
void exercise6 ();

int main ()
{
    
    exercise1();
    cout << endl;
    exercise2();
    cout << endl:
    exercise3();
    cout << endl;
    exercise4();
    cout << endl;
    exercise5();
    cout << endl;
    exercise6();
    
    return 0;
    
}

void exercise1 ()
{
    
    int input;
    
    cout << "Please Enter a number between 0 and 100: ";
    cin >> input;
    
    cout << "Output:" << endl;
    
    for (int i = input; i <= 100+input; i++)
    {
        cout << i << endl;
    }
    
}

void exercise2 ()
{
    
    cout << "All Upper and Lower case characters:" << endl;
    
    for (char i = 65; i < 123; i++) 
    {
        cout << i << endl;
    }
    
}

void exercise3 ()
{
    
    double base, power, temp;
    
    cout << "Enter the base number: ";
    cin >> base;
    cout << "Enter the exponent: ";
    cin >> power;
    
    temp = base;
    
    for (int i = 1; i < power; ++i)
    {
        base = base * temp;
    }
    
    cout << "The Number " << temp << " to the power of " << power << " is equal to " << base << endl;
    
}

void exercise4 () 
{
    
    int input, ans = 1;
    
    cout << "User Inputted: ";
    cin >> input;
    int limit = input;
    
    for (int i = 1; i <= limit; i++)
    {
        input = input * i;
        cout << i << "! = " << input << endl;
    }
    
}

void exercise5 ()
{
    
    int input;
    
    cout << "Please enter a number: ";
    cin >> input;
    
    for (int i = 1; i <= input; i++)
    {
        cout << "*";
        
        if (i % 5 == 0)
        {
            cout << endl;
        }
        
    }
    
    cout << endl;
    
}

void exercise6 ()
{
    
    int arr[5];
    
    cout << "Please Enter 5 numbers: " << endl;
    cin >> arr[0] >> arr[1] >> arr[2] >> arr[3] >> arr[4];
    
    for (int i = 0; i < 5; i++)
    {
        if (arr[i] % 2 == 0)
        {
            cout << "The number " << arr[i] << " is even." << endl;
        }
        else
        {
            cout << "The number " << arr[i] << " is odd." << endl;
        }
    }
    
}