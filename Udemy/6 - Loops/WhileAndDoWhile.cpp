#include <iostream>
using namespace std;

void exercise1 ();
void exercise2 ();

int main ()
{
    
    exercise1 ();
    cout << endl;
    exercise2 ();
    
    return 0;
    
}

void exercise1 ()
{
    
    int pin, check;
    
    cout << "User Inputted: " << endl;
    cout << "PIN: ";
    cin >> pin;
    
    do
    {

        cout << "CHECK PIN: ";
        cin >> check;
        
    } while (pin != check);
    
    cout << "Correct PIN!" << endl;
    
}

void exercise2 ()
{
    
    int pin, check, attempts = 5;
    
    cout << "User Inputted: " << endl;
    cout << "PIN: ";
    cin >> pin;
    
    do
    {

        cout << "CHECK PIN (" << attempts << " tries left): ";
        cin >> check;
        attempts--;
        
    } while (pin != check && attempts != 0);
    
    if (attempts == 0)
    {
        cout << "You have entered the wrong PIN code for 5 times!" << endl;
    }
    else
    {
        cout << "Correct PIN!" << endl;
    }
    
}