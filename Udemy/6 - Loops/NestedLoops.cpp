#include <iostream>
#include <stdlib.h>
using namespace std;

void exercise1();
void exercise2();
void exercise3();

int main ()
{
    
    //exercise1();
    //exercise2();
    exercise3();
    
    return 0;
    
}

void exercise1() {
    
    int arr[3][3];
    int count = 1;
    
    for (int i = 0; i < 3; i++) {
        
        for (int j = 0; j < 3; j++) {
            
            arr[i][j] = count;
            cout << arr[i][j] << "\t";
            count++;
            
        }
        
        cout << endl << endl << endl << endl;
        
    }
    
    system("clear");

}

void exercise2() {
    
    char arr[3][3];
    char count = '1';
    int x, y;
    int changed = 0;
    
    for (int i = 0; i < 3; i++) {
        
        for (int j = 0; j < 3; j++) {
            
            arr[i][j] = count;
            count++;
            
        }
        
    }
    
    do {
        
        for (int i = 0; i < 3; i++) {
            
            for (int j = 0; j < 3; j++) {
                
                cout << arr[i][j] << "\t";
                
            }
            
            cout << endl << endl << endl;
            
        }
        
        if (changed == 9) {
            
            cout << "All cells have been changed to X" << endl;
            break;
            
        }
        
        cout << "Please enter coordinates:" << endl;
        cout << "x coordinate: ";
        cin >> x;
        cout << "y coordinate: ";
        cin >> y;
        
        if (x > 3 || x < 1 || y > 3 || y < 1) {
            cout << "Invalid coordinates entered. Try again." << endl;
            system("clear");
            continue;
        }
        
        if (arr[x-1][y-1] == 'X') {
            
            system("clear");
            cout << "Coordinate has already been changed." << endl << endl;
            continue;
            
        } else {
        
            arr[x-1][y-1] = 'X';
            changed++;
            
        }
        
        system("clear");
        
    } while (true);
    
}

void exercise3() {
    
    char arr[3][3];
    char count = '1';
    int x, y;
    int changed = 0;
    bool p1 = true;
    
    for (int i = 0; i < 3; i++) {
        
        for (int j = 0; j < 3; j++) {
            
            arr[i][j] = count;
            count++;
            
        }
        
    }
    
    do {
        
        for (int i = 0; i < 3; i++) {
            
            for (int j = 0; j < 3; j++) {
                
                cout << arr[i][j] << "\t";
                
            }
            
            cout << endl << endl << endl;
            
        }
        
        if (changed == 9) {
            
            cout << "All cells have been changed" << endl;
            break;
            
        }
        
        if (p1 == true) {
            cout << "Player 1 please enter coordinates:" << endl;
            cout << "x coordinate: ";
            cin >> x;
            cout << "y coordinate: ";
            cin >> y;
        } else {
            cout << "Player 2 please enter coordinates:" << endl;
            cout << "x coordinate: ";
            cin >> x;
            cout << "y coordinate: ";
            cin >> y;
        }
        
        if (x > 3 || x < 1 || y > 3 || y < 1) {
            system("clear");
            cout << "Invalid coordinates entered. Try again." << endl;
            continue;
        }
        
        if (arr[x-1][y-1] == 'X' || arr[x-1][y-1] == 'O') {
            
            system("clear");
            cout << "Coordinate has already been changed." << endl << endl;
            continue;
            
        } else {
        
            if (p1 == true) {
                arr[x-1][y-1] = 'X';
                p1 = false;
            } else {
                arr[x-1][y-1] = 'O';
                p1 = true;
            }
            changed++;
            
        }
        
        system("clear");
        
    } while (true);
    
}