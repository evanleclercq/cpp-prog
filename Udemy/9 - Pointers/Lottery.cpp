#include <iostream>
#include <cstdlib>
using namespace std;

void bubbleSort (int *, int, int = 1);
void selectionSort (int *, int, int = 1);
void printArray (int *, int);
int * createArray (int);
void swapTwoValues (int *, int *);

int main() {
    
    int size;
    int order;
    char sort;
    
    cout << "Please enter the size of the array: ";
    cin >> size;
    
    int * array = createArray(size);
    
    do {
        
        cout << "Please enter the order to sort the array." << endl;
        cout << "1 = Ascending" << endl << "2 = Descending" << endl;
        cout << "Input: ";
        cin >> order;
        
        /*
        if (order != 1 || order != 2) {
            cout << "Invalid Selection Made. Try Again." << endl;
            continue;
        }
        */
        
        break;
        
    } while (true);
    
    do {
        
        cout << "Please enter the type of sort you want to use." << endl;
        cout << "b or B = Bubble Sort" << endl << "s or S = Selection Sort" << endl;
        cout << "Input: ";
        cin >> sort;
        
        /*
        if (sort != 'b' || sort != 'B' || sort != 'S' || sort != 's') {
            cout << "Invalid Selection Made. Try Again." << endl;
            continue;
        }
        */
        
        break;
        
    } while (true);
    
    if (sort == 'b' || sort == 'B') {
        bubbleSort(array, size, order);
    } else if (sort == 's' || sort == 'S') {
        selectionSort(array, size, order);
    }
    
    printArray(array, size);
    
    return 0;
    
}

void bubbleSort (int * array, int size, int order) {
    
    for (int i = 0; i < size-1; i++) {
        
        for (int j = 0; j < size-1; j++) {
            
            if (order == 1 && array[j] > array[j+1]) {
                swapTwoValues(array+j, array+j+1);
            } else if (order == 2 && array[j] < array[j+1]) {
                swapTwoValues(array+j, array+j+1);
            }
            
        }
        
    }
    
}

void selectionSort (int * array, int size, int order) {
    
    int index;
    
    for (int i = 0; i < size; i++) {
        
        index = i;
        
        for (int j = i; j < size; j++) {
            
            if (order == 1 && array[j] < array[index]) {
                index = j;
            } else if (order == 2 && array[j] > array[index]) {
                index = j;
            }
            
        }
        
        swapTwoValues(array+i, array+index);
        
    }
    
}

int * createArray (int size) {
    
    int * array = new int[size];
    
    for (int i = 0; i < size; i++) {
        
        array[i] = rand()%1500;
        
    }
    
    return array;
    
}

void printArray (int * arr, int size) {
    
    for (int i = 0; i < size; i++) {
        
        cout << "Element " << i << " = " << *(arr+i) << endl;
        
    }
    
}

void swapTwoValues (int * n, int * m) {
    
    int temp = *n;
    *n = *m;
    *m = temp;
    
}