#include <iostream>
using namespace std;

struct Book {
    string title;
    string author;
    int publicationYear;
};

void setBookData (Book &);
void printBookData (Book);

int main () {
    
    Book book1, book2, book3;
    
    setBookData(book1);
    setBookData(book2);
    setBookData(book3);
    
    printBookData(book1);
    printBookData(book2);
    printBookData(book3);
    
    return 0;
    
}

void setBookData (Book & book) {
    
    cout << endl;
    cout << "Setting Book Data: " << endl;
    cout << "Title: ";
    getline(cin, book.title);
    cout << "Author: ";
    getline(cin, book.author);
    cout << "Publication Year: ";
    cin >> book.publicationYear;
    cin.ignore();
    cout << endl;
    
}

void printBookData (Book book) {
    
    cout << endl;
    cout << "Showing Book Data:" << endl;
    cout << "Title: " << book.title << endl;
    cout << "Author: " << book.author << endl;
    cout << "Publication Year: " << book.publicationYear << endl;
    cout << endl;
    
}