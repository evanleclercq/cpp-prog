#include <iostream>
using namespace std;

void exercise1();

int main () {
    
    exercise1();
    
    return 0;
    
}

void exercise1() {
    
    int arr[5];
    int * p = arr;
    
    for (int i = 0; i < 5; i++) {
        
        *p = i * 6;
        cout << *p << endl;
    }
    
}