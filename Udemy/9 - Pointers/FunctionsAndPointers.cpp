#include <iostream>
using namespace std;

void exercise1();
void exercise2();
void exercise3();
void exercise4();
void exercise5();
void exercise6();

int * createArray (int);
void swapTwoValues (int *, int *);
void fillArrayWithValues (int *, int);
void printArrayValues (int *, int);
void bubbleSort (int *, int, int = 1);
void selectionSort (int *, int, int = 1);

int main () {
    
    //exercise1();
    //exercise2();
    //exercise3();
    //exercise4();
    //exercise5();
    exercise6();
    
    return 0;
    
}

void exercise1() {
    
    int * arr = createArray(10);
    
    for (int i = 0; i < 10; i++) {
        arr[i] = i + 9 * 3;
        cout << "arr[" << i << "] = " << arr[i] << endl;
    }
    
}

void exercise2() {
    
    int n = 20;
    int m = 50;
    
    cout << "Before Swap: " << endl << "n = " << n << endl << "m = " << m << endl << endl;
    
    swapTwoValues(&n, &m);
    
    cout << "After Swap: " << endl << "n = " << n << endl << "m = " << m << endl << endl;
    
}

void exercise3() {
    
    int n;
    
    cout << "Enter size of array: ";
    cin >> n;
    
    int * arr = new int[n];
    
    fillArrayWithValues(arr, n);
    printArrayValues(arr, n);
    
    delete arr;
    
}

void exercise4() {
    
    int arr[10] = {1, 5, 3, 12, 8, 2, 9, 21, 17, 11};
    
    bubbleSort(arr, 10, 2);
    printArrayValues(arr, 10);
    
}

void exercise5() {
    
    int arr[10] = {1, 5, 3, 12, 8, 2, 9, 21, 17, 11};
    
    selectionSort(arr, 10);
    printArrayValues(arr, 10);
    
}

void exercise6() {
    
    int sizeOfArray, order;
    char sort;
    
    cout << "Please Enter Size of the Array: ";
    cin >> sizeOfArray;
    
    int * arr = new int[sizeOfArray];
    
    fillArrayWithValues(arr, sizeOfArray);
    
    cout << "What order do you want to sort by (1 = asc, 2 = desc)? ";
    cin >> order;
    
    cout << "What type of sort do you want to use (b/B = Bubble, s/S = Selection? ";
    cin >> sort;
    
    if (sort == 'b' || sort == 'B') {
        bubbleSort(arr, sizeOfArray, order);
    } else if (sort == 's' || sort == 'S') {
        selectionSort(arr, sizeOfArray, order);
    }
    
    printArrayValues(arr, sizeOfArray);
    
    delete arr;
    
}

int * createArray (int n) {
    
    int * newArray = new int[n];
    return newArray;
    
}

void swapTwoValues (int * n, int * m) {
    
    int temp = *n;
    *n = *m;
    *m = temp;
    
}

void fillArrayWithValues (int * arr, int n) {
    
    for (int i = 0; i < n; i++) {
        
        cout << "Enter " << i+1 << " Element: ";
        cin >> arr[i];
        
    }
    
}

void printArrayValues (int * arr, int n) {
    
    for (int i = 0; i < n; i++) {
        
        cout << "Number " << i << " = " << *(arr+i) << endl;
        
    }
    
}

void bubbleSort (int * arr, int n, int order) {
    
    for (int i = 0; i < n-1; i++) {
        
        for (int j = 0; j < n-1; j++) {
            
            if (order == 1 && arr[j] > arr[j+1]) {
                swapTwoValues(arr+j, arr+j+1);
            } else if (order == 2 && arr[j] < arr[j+1]) {
                swapTwoValues(arr+j, arr+j+1);
            }
            
        }
        
    }
    
}

void selectionSort (int * arr, int n, int order) {
    
    int index;
    
    for (int i = 0; i < n; i++) {
        
        index = i;
        
        for (int j = i; j < n; j++) {
            
            if (order == 1 && arr[j] < arr[index]) {
                index = j;
            } else if (order == 2 && arr[j] > arr[index]) {
                index = j;
            }
            
        }
        
        swapTwoValues(arr+i, arr+index);
        
    }
    
}