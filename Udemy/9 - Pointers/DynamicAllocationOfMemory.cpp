#include <iostream>
using namespace std;

void exercise1();
void exercise2();

int main () {
    
    //exercise1();
    exercise2();
    
    return 0;
    
}

void exercise1() {
    
    int a;
    int * b = new int;
    int * p = &a;
    
    *p = 10;
    *b = 15;
    
    cout << "The value of the statically allocated variable: " << *p << ", and the address: " << p << endl;
    cout << "The value of the dynamically allocated variable: " << *b <<  ", and the address: " << b << endl;
    
    delete b;
    
}

void exercise2() {
    
    int * a = new int;
    int n;
    
    cout << "Please enter a number: ";
    cin >> n;
    
    for (int i = 1; i <= n; i++) {
        *a = 15 * (i+2);
        cout << "Value at Address: " << a << " = " << *a << endl;
        a++;
    }
    
}