#include <iostream>
using namespace std;

void exercise1();
void exercise2();
void exercise3();

int main() {
    
    exercise1();
    exercise2();
    exercise3();
    
    return 0;
    
}

void exercise1() {
    
    char c[27] = "abcdefghijklmnopqrstuvwxyz";
    
    for (int i = 0; i < 26; i++){
        cout << c[i];
    }
    
    cout << endl;
    
}

void exercise2() {
    
    string s = "abcdefghijklmnopqrstuvwxyz";
    
    for (int i = 0; i < 26; i++){
        cout << s[i];
    }
    
    cout << endl;
    
}

void exercise3() {
    
    int num;
    
    cout << "Please Enter number of characters: ";
    cin >> num;
    
    char * characters = new char[num];
    
    for (int i = 1; i <= num; i++) {
        
        cout << "Enter number " << i << ": ";
        cin >> characters[i];
        
    }
    
    cout << "The inputted characters as a string: ";
    
    for (int i = 0; i < num; i++) {
        cout << *(characters+i);
    }
    
    cout << endl;
    
    delete characters;
    
}