//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-variables_datatypes_exercises.htm

#include <iostream>
#include <string>

using namespace std;

void exercise1();
void exercise2();
void exercise3();
void exercise4();
void exercise5();

int main () {

    exercise1();
    cout << endl << endl;
    exercise2();
    cout << endl << endl;
    exercise3();
    cout << endl << endl;
    exercise4();
    cout << endl << endl;
    exercise5();
    
    return 0;
    
}

void exercise1 () {

    int age = 10;
    
    cout << "You are " << age << " years old." << endl;
    cout << "You are too young to place this game." << endl;
    
}

void exercise2 () {
    
    for (int i = 0; i < 5; i++) {
        
        cout << "*****" << endl;
        
    }
    
}

void exercise3 () {
    
    int n = 10;
    int m = 15;
    float f = 12.6;
    
    cout << "int 1 = " << n << endl;
    cout << "int 2 = " << m << endl;
    cout << "float = " << f << endl;
    
}

void exercise4 () {
    
    string name;
    
    cout << "Please enter your name: ";
    cin >> name;
    
    cout << endl << "Hello, " << name << endl;
    
}

void exercise5 () {
    
    int arr[3];
    
    cout << "Please Enter Three Numbers: ";
    cin >> arr[0] >> arr[1] >> arr[2];
    
    cout << endl << "Your numbers in forward order:" << endl;
    
    for (int i = 0; i < 3; i++) {
        cout << arr[i] << endl;
    }
    
    cout << endl << "Your numbers in reverse order:" << endl;
    
    for (int i = 2; i >= 0; i--) {
        cout << arr[i] << endl;
    }
    
}
