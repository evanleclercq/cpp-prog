//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-functions_exercises.htm

#include <iostream>

using namespace std;

void exercise1();
double addition(double, double);
double subtraction(double, double);
double multiplication(double, double);
double division(double, double);
int mod(double, double);

int main () {
    
    exercise1();
    
    return 0;
    
}

void exercise1 () {
    
    int n;
    double x, y;
    
    cout << "====================================" << endl;
    cout << "CALCULATOR MENU" << endl;
    cout << "====================================" << endl;
    
    cout << "1. Addition" << endl;
    cout << "2. Subtraction" << endl;
    cout << "3. Multiplication" << endl;
    cout << "4. Division" << endl;
    cout << "5. Modulus" << endl << endl;
    cout << "Please make a selection: ";
    cin >> n;
    
    cout << endl << "Please enter your two numbers separated by a space: ";
    cin >> x >> y;
    
    switch (n) {
        case 1:
            cout << endl << "ANSWER" << endl << x << " + " << y << " = " << addition(x, y) << endl;
            break;
        case 2:
            cout << endl << "ANSWER" << endl << x << " - " << y << " = " << subtraction(x, y) << endl;
            break;
        case 3:
            cout << endl << "ANSWER" << endl << x << " * " << y << " = " << multiplication(x, y) << endl;
            break;
        case 4:
            cout << endl << "ANSWER" << endl << x << " / " << y << " = " << division(x, y) << endl;
            break;
        case 5:
            cout << endl << "ANSWER" << endl << x << " % " << y << " = " << mod(x, y) << endl;
            break;
        default:
            cout << "Invalid Selection" << endl;
            break;
    }
    
}

double addition(double x, double y) {
    return (x + y);
}
double subtraction(double x, double y) {
    return (x - y);
}
double multiplication(double x, double y) {
    return (x * y);
}
double division(double x, double y) {
    return (x / y);
}
int mod(double x, double y) {
    return ((int)x % (int)y);
}