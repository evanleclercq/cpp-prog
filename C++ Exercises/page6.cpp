//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-more_conditional_statements_exercises.htm

#include <iostream>

using namespace std;

void exercise1();

int main () {
    
    exercise1();
    
    return 0;
    
}

void exercise1() {
    
    int a, b, c;
    double x, x1;
    
    cout << "Please Enter a value for a: ";
    cin >> a;
    cout << "Please Enter a value for b: ";
    cin >> b;
    cout << "Please Enter a value for c: ";
    cin >> c;
    
    if (a == 0 && b == 0) {
        
        cout << "There are no roots." << endl;
        
    } else if (((b*b)-(4*(a*c))) < 0) {
        
        cout << "There are no roots." << endl;
        
    } else if (a == 0) {
        
        cout << "There is one root." << endl;
        cout << "Root = " << (-1 * c) / b << endl;
        
    } else {
        
        cout << "There are two roots." << endl;
        
        x1 = (b + ((b * b) - ((4 * (a * c)) * 0.5)) ) / (2 * a);
        x = (b - ((b * b) - ((4 * (a * c)) * 0.5)) ) / (2 * a);
        
        cout << "Root x = " << x << endl;
        cout << "Root x1 = " << x1 << endl;
    }
    
}