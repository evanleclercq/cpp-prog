//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-operators_increment_decrement_exercises.htm

#include <iostream>

using namespace std;

void exercise1();
void exercise2();

int main () {
    
    exercise1 ();
    cout << endl << endl;
    exercise2 ();
    
    return 0;
    
}

void exercise1() {
    
    int x;
    
    cout << "Start x" << "\t" << "expression" << "\t" << "expression value" << "\t" << "end x" << endl;
    
    x = 5;
    cout << x << "\t" << "x++" << "\t" << x++ << "\t" << x << endl;
    
    x = 5;
    cout << x << "\t" << "x--" << "\t" << x-- << "\t" << x << endl;
    
    x = 5;
    cout << x << "\t" << "++x" << "\t" << ++x << "\t" << x << endl;

    x = 5;
    cout << x << "\t" << "--x" << "\t" << --x << "\t" << x << endl;
    
}

void exercise2() {
    
    int a;
    
    cout << "Please enter a number: ";
    cin >> a;
    cout << "You entered " << a << endl << endl;
    
    cout << "The value of ++a is: " << ++a << endl;
    cout << "Now the value of a is: " << a << endl;
    
    cout << "The value of a++ is: " << a++ << endl;
    cout << "Now the value of a is: " << a << endl;
    
    cout << "The value of --a is: " << --a << endl;
    cout << "Now the value of a is: " << a << endl;
    
    cout << "The value of a-- is: " << a-- << endl;
    cout << "Now the value of a is: " << a << endl;
    
}