//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-loop_exercises.htm

#include <iostream>

using namespace std;

void exercise1();
void exercise2();

int main () {
    
    exercise1();
    cout << endl << endl;
    exercise2();
    
    return 0;
    
}

void exercise1() {
    
    for (int i = 8; i >= 0; i--) {
        
        for (int j = i; j >= 0; j--) {
            
            cout << "* ";
            
        }
        
        cout << endl;
        
    }
    
}

void exercise2() {
    
    for (int i = 1; i <= 8; i++) {
        
        for (int j = 1; j <= i; j++) {
            
            cout << j << " ";
            
        }
        
        for (int k = 8-i; k > 0; k--) {
            
            cout << "* ";
            
        }
        
        cout << endl;
        
    }
    
}