//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-operators_arithmetic_compound_exercises.htm

#include <iostream>
#include <string>

using namespace std;

void exercise1();
void exercise2();
void exercise3();

int main () {
    
    exercise1();
    cout << endl << endl;
    exercise2();
    cout << endl << endl;
    exercise3();
    
    return 0;
    
}

void exercise1() {
    
    int x = 10, y = 5;
    
    cout << "x value" << "\t" <<  "y value" << "\t" << "expression" << "\t" << "result" << endl;

    cout << x << " \t" << y <<  " \t"  << "x = y + 3" << " \t" << (y+3) << endl;
    cout << x << " \t" << y <<  " \t"  << "x = y - 2" << " \t" << (y-2) << endl;
    cout << x << " \t" << y <<  " \t"  << "x = y * 5" << " \t" << (y*5) << endl;
    cout << x << " \t" << y <<  " \t"  << "x = x / y" << " \t" << (x/y) << endl;
    cout << x << " \t" << y <<  " \t"  << "x = x % y" << " \t" << (x%y) << endl;

}

void exercise2() {
    
    int x = 10, y = 5;
    
    cout << "x value" << "\t" <<  "y value" << "\t" << "expression" << "\t" << "result" << endl;   
    
    cout << x << " \t" << y <<  " \t"  << "x += y" << " \t" << "\t" << (x+y) << endl;
    cout << x << " \t" << y <<  " \t"  << "x -= y - 2" << " \t" << (x-(y-2)) << endl;
    cout << x << " \t" << y <<  " \t"  << "x = y * 5" << " \t" << (x*(y*5)) << endl;
    cout << x << " \t" << y <<  " \t"  << "x = x / y" << " \t" << (x/(x/y)) << endl;
    cout << x << " \t" << y <<  " \t"  << "x = x % y" << " \t" << (x%y) << endl;
    
}

void exercise3 () {
    
    for (int i = 8; i > 0; i--) {
        
        for (int j = i; j > 0; j--) {
            
            cout << "* ";
            
        }
        
        cout << endl;
        
    }
    
}