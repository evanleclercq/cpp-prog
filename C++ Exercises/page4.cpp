//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-more_operators_exercises.htm

#include <iostream>

using namespace std;

void exercise1();
void exercise2();

int main () {
    
    exercise1();
    cout << endl << endl;
    exercise2();
    
    return 0;
    
}

void exercise1() {
    
    int quiz[3];
    int mid;
    int final;
    
    cout << "===============QUIZZES===============" << endl;
    cout << "Enter the score for the first quiz: ";
    cin >> quiz[0];
    cout << "Enter the score for the second quiz: ";
    cin >> quiz[1];
    cout << "Enter the score for the third quiz: ";
    cin >> quiz[2];
    cout << "===============MID-TERM===============" << endl;
    cout << "Enter the score for the mid-term: ";
    cin >> mid;
    cout << "===============FINAL===============" << endl;
    cout << "Etner the score for the final: ";
    cin >> final;
    cout << "===============RESULTS===============" << endl;
    cout << "Quiz total: " << quiz[0] + quiz[1] + quiz[2] << endl;
    cout << "Mid-term: " << mid << endl;
    cout << "Final: " << final << endl;
    cout << "===============TOTAL===============" << endl;
    cout << "Total: " << mid + final + quiz[0] + quiz[1] + quiz[2] << endl;
    
    
}

void exercise2() {
    
    int x, y, p, s, total;
    
    cout << "Enter value for x: ";
    cin >> x;
    cout << "Enter value for y: ";
    cin >> y;
    
    p = x * y;
    s = x + y;
    
    total = (s*s) + p * (s - x) * (p + y);
    
    cout << endl << "Total: " << total << endl << endl;
    
}