//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-conditional_statements_exercises.htm

#include <iostream>

using namespace std;

void exercise1();
void exercise2();

int main () {
    
    exercise1();
    cout << endl << endl;
    exercise2();
    
    return 0;
    
}

void exercise1() {
    
    int a, b, c, max;
    
    cout << "Please enter three numbers seperated by a space: ";
    cin >> a >> b >> c;
    
    max = a;
    
    if (max < b) max = b;
    if (max < c) max = c;
    
    cout << "The highest number you entered was: " << max << endl;
    
}

void exercise2() {
    
    int quiz, mid, final, avg;
    
    cout << "Please enter the grade for the quizzes: ";
    cin >> quiz;
    cout << "Please enter the grade for the mid-term: ";
    cin >> mid;
    cout << "Please enter the grade for the final: ";
    cin >> final;
    
    avg = (quiz + mid + final) / 3;
    cout << "Your final score is:" << avg << endl;
    
    if (avg >= 90) {
        cout << "Your final grade is: A" << endl;
    } else if (avg >= 70) {
        cout << "Your final grade is: B" << endl;
    } else if (avg >= 50) {
        cout << "Your final grade is: C" << endl;
    } else {
        cout << "Your final grade is: F" << endl;
    }
    
}