//Link for exercises: http://www.worldbestlearningcenter.com/index_files/cpp-tutorial-more_functions_exercises.htm

#include <iostream>

using namespace std;

void exercise1();
void exercise2();

int main () {
    
    exercise1();
    cout << endl << endl;
    exercise2();
    
    return 0;
    
}

void exercise1() {
    
    for (int i = 1; i <= 12; i++) {
        
        for (int j = 1; j <= 12; j++) {
            
            cout << i << " * " << j << " = " << (i * j) << "\t";
            
        }
        
        cout << endl;
        
    }
    
}

void exercise2() {
    
    double a, b, c, max, min;
    
    cout << "Enter value for a: ";
    cin >> a;
    cout << "Enter value for b: ";
    cin >> b;
    cout << "Enter value for c: ";
    cin >> c;
    
    max = min = a;
    
    if (max < b) max = b;
    if (max < c) max = c;
    if (min > b) min = b;
    if (min > c) min = c;
    
    cout << endl;
    cout << "Minimum Value: " << min << endl;
    cout << "Maximum Value: " << max << endl;
    
    
}